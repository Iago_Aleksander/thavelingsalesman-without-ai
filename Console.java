
import java.util.Scanner;

// A better way to read information from the user. 
// Uses the scanner class from the java.util package as a basis.
public class Console {
	public static Scanner console = new Scanner(System.in);

	public static String askString (String aPrompt)
	{
		System.out.print(aPrompt);
		String reply = console.nextLine();
		return reply;
	}
	public static int askInt (String aPrompt)
	{
		String reply = askString(aPrompt);
		return Integer.parseInt(reply);
	}
	public static double askDouble(String aPrompt)
	{
		String reply = askString (aPrompt);
		return Double.parseDouble(reply);
	}
	public static char askOption (String aMenu)
	{
		System.out.println(aMenu);
		String reply = askString("Enter option: ");
		if (reply.trim().length() == 0)
			return '\0';
		else
			return Character.toUpperCase(reply.trim().charAt(0));
	}
	public static char askChar(String aChar)
	{
		String reply = askString(aChar);
		return Character.toUpperCase(reply.trim().charAt(0));
	}
}
